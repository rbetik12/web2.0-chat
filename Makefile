all:
	$(CC) server.c -std=c11 -g -fsanitize=address -O3 -fno-omit-frame-pointer -Werror -Wall -Wextra -pthread -pedantic -o server
	$(CC) client.c -std=c11 -g -fsanitize=address -O3 -fno-omit-frame-pointer -Werror -Wall -Wextra -pthread -pedantic -o client

clean:
	rm -f server
	rm -f client
