#pragma once

#define CHECK(X, MSG) if (X) {ExitWithError(MSG);}

#include <stdlib.h>

void ExitWithError(const char *errorMessage)
{
    perror(errorMessage);
    exit(EXIT_FAILURE);
}

bool TcpSend(int socket, void *buffer, size_t size, int flags)
{
    ssize_t sendAmount = 0;
    ssize_t sizeToSend = size;
    ssize_t bufferOffset = 0;
    while ((sendAmount = send(socket, (char *) buffer + bufferOffset, sizeToSend, flags)) != sizeToSend)
    {
        if (sendAmount == -1)
        {
            return false;
        }
        printf("Send() didn't send all bytes. Bytes send %zd/%zd\n", sendAmount, sizeToSend);
        sizeToSend -= sendAmount;
        bufferOffset += sendAmount;
    }

    return true;
}

bool SendString(const char *string, int socket, bool requireSwap)
{
    // We convert stringLength to big-endian format, because TCP/IP standard network byte-order is big-endian
    int stringLength = 0;
    bool sendResult;
    if (requireSwap)
    {
        stringLength = htonl(strlen(string));
    }

    sendResult = TcpSend(socket, &stringLength, sizeof(stringLength), MSG_NOSIGNAL);
    if (!sendResult)
    {
        fprintf(stderr, "Can't send size of message <%s>. Server is down.\n", string);
        return false;
    }

    if (requireSwap)
    {
        stringLength = htonl(stringLength);
    }
    sendResult = TcpSend(socket, (void *) string, stringLength, MSG_NOSIGNAL);

    if (!sendResult)
    {
        fprintf(stderr, "Message <%s> wasn't sent. Server is down.\n", string);
        return false;
    }

    return true;
}

bool ReceiveString(char **string, int clientSocket, bool requireSwap)
{
    int stringLength = 0;
    ssize_t readAmount = recv(clientSocket, &stringLength, sizeof(stringLength), MSG_WAITALL | MSG_NOSIGNAL);

    if (readAmount == -1)
    {
        fprintf(stderr, "Can't receive message size. Receive error.\n");
        perror("ReceiveString message size error");
        return false;
    } else if (readAmount == 0)
    {
        return false;
    }

    if (requireSwap)
    {
        stringLength = ntohl(stringLength);
    }

    *string = calloc(stringLength + 1, sizeof(char));

    readAmount = recv(clientSocket, *string, stringLength, MSG_WAITALL | MSG_NOSIGNAL);

    if (readAmount == -1)
    {
        fprintf(stderr, "Message wasn't received. Receive error.\n");
        return false;
    } else if (readAmount == 0)
    {
        return false;
    }

    return true;
}

struct ClientWorkerInfo
{
    int clientSocket;
    bool requireSwap;
};

struct ClientWorkerInfoList
{
    struct ClientWorkerInfoList *next;
    struct ClientWorkerInfo *element;
};

void ListAdd(struct ClientWorkerInfoList **listHead, struct ClientWorkerInfo *element)
{
    struct ClientWorkerInfoList *it = *listHead;
    if (it == NULL)
    {
        *listHead = calloc(1, sizeof(struct ClientWorkerInfoList));
        it = *listHead;
        it->element = calloc(1, sizeof(struct ClientWorkerInfo));
        memcpy(it->element, element, sizeof(struct ClientWorkerInfo));
        return;
    }

    while (it->next != NULL)
    {
        it = it->next;
    }

    it->next = calloc(1, sizeof(struct ClientWorkerInfoList));
    it->next->element = calloc(1, sizeof(struct ClientWorkerInfoList));
    memcpy(it->next->element, element, sizeof(struct ClientWorkerInfo));
}

void ListRemove(struct ClientWorkerInfoList *listHead, struct ClientWorkerInfoList *element)
{
    struct ClientWorkerInfoList *listIt = listHead;

    while (listIt->next == element)
    {
        listIt = listIt->next;
    }

    listIt->next = element->next;
    element->next = NULL;
    free(element->element);
    free(element);
}

struct ClientWorkerInfo *GetLast(struct ClientWorkerInfoList *listHead)
{
    if (listHead == NULL)
    {
        return NULL;
    }

    struct ClientWorkerInfoList *info = listHead;

    while (info->next != NULL)
    {
        info = info->next;
    }

    return info->element;
}
