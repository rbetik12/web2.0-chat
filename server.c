#include <stdio.h>
#include <sys/socket.h>
#include <asm-generic/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include "common.h"
#include <signal.h>
#include <pthread.h>
#include <sys/queue.h>
#include <stdlib.h>

#define MAX_CLIENTS_AMOUNT 8

static int serverSocket = 0;
static bool isRunning = true;

struct BroadcastMessage
{
    TAILQ_ENTRY(BroadcastMessage) tailQ;
    char *nickname;
    char *message;
};

TAILQ_HEAD(BroadcastMessageQueue, BroadcastMessage);
struct BroadcastMessageQueue messageQueue;
struct ClientWorkerInfoList* clientList = NULL;

pthread_mutex_t messageQueueMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t clientListMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t newMessageConVar = PTHREAD_COND_INITIALIZER;

void StopSignalHandle()
{
    isRunning = false;
    close(serverSocket);
    serverSocket = 0;
}

void *BroadcastWorker()
{
    while (isRunning)
    {
        pthread_mutex_lock(&messageQueueMutex);
        pthread_cond_wait(&newMessageConVar, &messageQueueMutex);
        while (!TAILQ_EMPTY(&messageQueue))
        {
            struct BroadcastMessage *queueMessage = TAILQ_FIRST(&messageQueue);

            struct timespec timestamp;
            clock_gettime(CLOCK_MONOTONIC, &timestamp);
            struct tm *mTm = localtime(&timestamp.tv_sec);
            char timestampStr[15];
            strftime(timestampStr, 15, "%X", mTm);

            printf("[%s]{%s}: %s\n", timestampStr, queueMessage->nickname, queueMessage->message);
            struct ClientWorkerInfoList *listIt = clientList;

            pthread_mutex_lock(&clientListMutex);
            bool isOk = true;
            while(listIt != NULL)
            {
                if (listIt->element->clientSocket == -1)
                {
                    goto next;
                }
                isOk = SendString(queueMessage->nickname, listIt->element->clientSocket, listIt->element->requireSwap);
                if (!isOk)
                {
                    goto invalidSocket;
                    //TODO free memory from ClientWorkerInfo
                }
                isOk = SendString(queueMessage->message, listIt->element->clientSocket, listIt->element->requireSwap);
                if (!isOk)
                {
                    goto invalidSocket;
                    //TODO free memory from ClientWorkerInfo
                }
                isOk = SendString(timestampStr, listIt->element->clientSocket, listIt->element->requireSwap);
                if (!isOk)
                {
                    goto invalidSocket;
                    //TODO free memory from ClientWorkerInfo
                }
                next:
                listIt = listIt->next;
                continue;

                invalidSocket:
//                close(listIt->element->clientSocket);
                listIt->element->clientSocket = -1;
            }
            pthread_mutex_unlock(&clientListMutex);

            TAILQ_REMOVE(&messageQueue, queueMessage, tailQ);
            free(queueMessage->message);
            free(queueMessage->nickname);
            free(queueMessage);
        }

        pthread_mutex_unlock(&messageQueueMutex);

//        struct ClientWorkerInfoList* listIt = clientList;
//        while (listIt != NULL)
//        {
//            if (listIt->element->clientSocket == -1)
//            {
//                ListRemove(clientList, listIt);
//                listIt = clientList;
//                continue;
//            }
//            listIt = listIt->next;
//        }
    }

    return NULL;
}

void *ClientWorker(void *workerDataPtr)
{
    struct ClientWorkerInfo *workerData = (struct ClientWorkerInfo *) workerDataPtr;
    char *nickname = NULL;
    char *message = NULL;

    bool isOk = true;
    while (1)
    {
        isOk = ReceiveString(&nickname, workerData->clientSocket, workerData->requireSwap);
        if (!isOk)
        {
            break;
        }
        isOk = ReceiveString(&message, workerData->clientSocket, workerData->requireSwap);
        if (!isOk)
        {
            break;
        }

        struct BroadcastMessage *queueMessage = calloc(1, sizeof(struct BroadcastMessage));
        queueMessage->nickname = nickname;
        queueMessage->message = message;

        pthread_mutex_lock(&messageQueueMutex);
        TAILQ_INSERT_HEAD(&messageQueue, queueMessage, tailQ);
        pthread_cond_signal(&newMessageConVar);
        pthread_mutex_unlock(&messageQueueMutex);
    }

//    free(workerDataPtr);

    return NULL;
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Not enough arguments!");
        exit(EXIT_FAILURE);
    }
    signal(SIGTERM, StopSignalHandle);
    signal(SIGINT, StopSignalHandle);
    signal(SIGKILL, StopSignalHandle);

    TAILQ_INIT(&messageQueue);

    bool requireSwap = true;

    if (1 == htonl(1))
    {
        requireSwap = false;
    }

    struct sockaddr_in serverAddress;
    int socketOptions = 0;
    char *end;
    short port = strtol(argv[1], &end, 10);

    memset(&serverAddress, 0, sizeof(struct sockaddr_in));

    CHECK(!(serverSocket = socket(AF_INET, SOCK_STREAM, 0)), "Can't create socket!")
    CHECK(setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &socketOptions, sizeof(socketOptions)),
          "Can't set socket options!")

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddress.sin_port = htons(port);

    CHECK(bind(serverSocket, (const struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0, "Can't bind socket!")
    CHECK(listen(serverSocket, MAX_CLIENTS_AMOUNT) < 0, "Can't start listening!")

    printf("Server listening to 0.0.0.0:%d!\n", port);

    int newClientSocket = 0;

    pthread_t broadcastWorkerThreadId;
    pthread_create(&broadcastWorkerThreadId, NULL, &BroadcastWorker, NULL);
    pthread_detach(broadcastWorkerThreadId);

    while (isRunning)
    {
        newClientSocket = accept(serverSocket, NULL, NULL);
        struct ClientWorkerInfo *workerData = calloc(1, sizeof(struct ClientWorkerInfo));
        workerData->clientSocket = newClientSocket;
        workerData->requireSwap = requireSwap;
        pthread_mutex_lock(&clientListMutex);
        ListAdd(&clientList, workerData);
        pthread_mutex_unlock(&clientListMutex);
        pthread_t threadId;
        pthread_create(&threadId, NULL, &ClientWorker, GetLast(clientList));
        pthread_detach(threadId);
        free(workerData);
    }

    if (serverSocket)
    {
        close(serverSocket);
    }

    return 0;
}
