#define _POSIX_C_SOURCE    200809L

#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include "common.h"
#include <pthread.h>
#include <strings.h>
#include <netdb.h>

#define MAX_MESSAGE_LENGTH 1024
static int sendSocket = 0;
static struct sockaddr *sockaddr;
bool requireSwap = true;
bool isInputMode = false;
bool isEOF = false;

void LogSuccessfulConnection(short port)
{
    char addressStr[40];
    printf("Successfully connected to server at %s:%d!\n",
           inet_ntop(AF_INET, &sockaddr, addressStr, sizeof(addressStr)),
           port);
}

void ConnectToServer(char *address, short port)
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    int s;
    if ((s = getaddrinfo(address, NULL, &hints, &result)))
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }

    for (rp = result; rp != NULL; rp = rp->ai_next)
    {
        sendSocket = socket(rp->ai_family, rp->ai_socktype,
                            rp->ai_protocol);
        if (sendSocket == -1)
        {
            continue;
        }

        ((struct sockaddr_in *) (rp->ai_addr))->sin_port = htons(port);
        if (connect(sendSocket, rp->ai_addr, rp->ai_addrlen) != -1)
        {
            sockaddr = rp->ai_addr;
            break;
        }

        close(sendSocket);
    }

    freeaddrinfo(result);

    if (rp == NULL)
    {
        fprintf(stderr, "Could not connect to server\n");
        exit(EXIT_FAILURE);
    }
}

void GetMessage(char **message)
{
    char input = getchar();
    getchar();
    if (input == 'm')
    {
        isInputMode = true;
    } else
    {
        isEOF = true;
        return;
    }

    const int terminatedMessageSize = MAX_MESSAGE_LENGTH + 1;
    *message = calloc(terminatedMessageSize, sizeof(char));
    if (!fgets(*message, terminatedMessageSize, stdin))
    {
        ExitWithError("Fgets error!");
    }

    char *str = *message;
    for (size_t i = 0; i < MAX_MESSAGE_LENGTH; i++)
    {
        if (str[i] == '\n')
        {
            str[i] = '\0';
            break;
        }
    }

    isInputMode = false;
}

void *ReceiveWorker()
{
    char *nickname = NULL;
    char *message = NULL;
    char *timestamp = NULL;
    bool isOk = true;
    while (1)
    {
        isOk = ReceiveString(&nickname, sendSocket, requireSwap);
        if (!isOk)
        {
            return NULL;
        }
        isOk = ReceiveString(&message, sendSocket, requireSwap);
        if (!isOk)
        {
            return NULL;
        }
        isOk = ReceiveString(&timestamp, sendSocket, requireSwap);
        if (!isOk)
        {
            return NULL;
        }

        if (!isInputMode)
        {
            printf("[%s]{%s}: %s\n", timestamp, nickname, message);
        }
        free(timestamp);
        free(nickname);
        free(message);
    }
}

int main(int argc, char *argv[])
{
    if (argc < 4)
    {
        fprintf(stderr, "Not enough arguments!");
        exit(EXIT_FAILURE);
    }
    if (1 == htonl(1))
    {
        requireSwap = false;
    }

    char *address, *end;
    address = argv[1];
    short port = strtol(argv[2], &end, 10);
    char *nickname = argv[3];
    char *message = NULL;
    ConnectToServer(address, port);

    LogSuccessfulConnection(port);

    pthread_t receiveThreadId;
    pthread_create(&receiveThreadId, NULL, &ReceiveWorker, NULL);
    pthread_detach(receiveThreadId);

    bool isOk = true;
    while (1)
    {
        GetMessage(&message);
        if (isEOF)
        {
            sleep(1);
            break;
        }
        if (message)
        {
            isOk = SendString(nickname, sendSocket, requireSwap);
            if (!isOk)
            {
                break;
            }
            isOk = SendString(message, sendSocket, requireSwap);
            if (!isOk)
            {
                break;
            }
            free(message);
            message = NULL;
        }
    }

    close(sendSocket);

    return 0;
}
